﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardReader : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreDisplaytext;

    public bool hasDouble;
    public bool hasSplit;

    public bool canDoubleDown;

    public int currentScore;
    public List<GameObject> hand = new List<GameObject>();

    public bool isBust;
    public bool isBlackjack;

    private int latestCardValue;

    private void Awake()
    {
        ResetScore();
    }

    private void Update()
    {
        if (hand.Count == 2 && hand[0].GetComponent<CardManager>().type == hand[1].GetComponent<CardManager>().type && !hasSplit)
        {
            hasDouble = true;
        }
        else
        {
            hasDouble = false;
        }

        if (hand.Count == 2 && currentScore >= 9 && currentScore <=11)
        {
            canDoubleDown = true;
        }
        else
        {
            canDoubleDown = false;
        }
    }

    public void UpdateScore()
    {
        hand.Clear();
        foreach (Transform card in transform)
        {
            hand.Add(card.gameObject);
        }

        currentScore = 0;
        for (int i = 0; i < hand.Count; i++)
        {
            currentScore += hand[i].GetComponent<CardManager>().cardValue;
            DisplayNewScore();
        }
    }

    public void DecreaseScore()
    {
        GetLatestCard();
        currentScore -= latestCardValue;
        DisplayNewScore();
    }

    public void IncreaseScore()
    {
        GetLatestCard();
        currentScore += latestCardValue;
        DisplayNewScore();
    }


    private void GetLatestCard()
    {
        latestCardValue = transform.GetChild(transform.childCount - 1).GetComponentInChildren<CardManager>().cardValue;
    }

    public void ResetScore()
    {
        currentScore = 0;
        DisplayNewScore();
    }

    public void DisplayNewScore()
    {
        scoreDisplaytext.text = currentScore.ToString();
    }
}
