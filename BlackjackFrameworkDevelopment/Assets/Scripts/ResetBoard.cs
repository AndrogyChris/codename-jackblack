﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBoard : MonoBehaviour
{
    [SerializeField] Transform deck;
    [SerializeField] List<GameObject> aces = new List<GameObject>();

    CardDealer cardDealer;
    ButtonManager buttonManager;

    private void Awake()
    {
        cardDealer = FindObjectOfType<CardDealer>();
        buttonManager = FindObjectOfType<ButtonManager>();
    }

    public void StartNewRound()
    {
        for (int i = 0; i < cardDealer.players.Count; i++)
        {
            cardDealer.currentPlayer = i;
            ReturnPlayerCardsToDeck();

            if (cardDealer.players[cardDealer.currentPlayer].GetComponent<CardReader>().hasSplit)
            {
                cardDealer.UnSplitHand();
            }
        }

        ReturnDealerCardsToDeck();
        buttonManager.StartRoundButtonTrue();
        buttonManager.SetPlayerIndicatorFalse();
    }

    private void ReturnPlayerCardsToDeck()
    {
        CardReader cardReader = cardDealer.players[cardDealer.currentPlayer].GetComponent<CardReader>();

        foreach (GameObject card in cardReader.hand)
        {
            card.transform.SetParent(deck);
            card.transform.localPosition = new Vector3(0, 0, 0);
        }

        foreach (GameObject ace in aces)
        {
            
            ace.GetComponent<CardManager>().ReturnAce();
        }

        cardReader.ResetScore();
    }

    private void ReturnDealerCardsToDeck()
    {
        CardReader cardReader = cardDealer.dealer.GetComponent<CardReader>();

        foreach (GameObject card in cardReader.hand)
        {
            card.transform.SetParent(deck);
            card.transform.localPosition = new Vector3(0, 0, 0);
        }

        cardReader.ResetScore();
    }

}
