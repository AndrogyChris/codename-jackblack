﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareScores : MonoBehaviour
{
    [SerializeField] GameObject playerTurnImage;
    [SerializeField] GameObject bustText;
    [SerializeField] GameObject winText;
    [SerializeField] GameObject loseText;
    [SerializeField] GameObject tieText;
    [SerializeField] GameObject blackJackText;

    [SerializeField] GameObject dealerBlackjackText;
    [SerializeField] GameObject dealerBustText;

    [SerializeField] float compareScoresDelay = 2f;

    public bool comparing = false;

    CardDealer cardDealer;
    ResetBoard resetBoard;

    private void Awake()
    {
        cardDealer = FindObjectOfType<CardDealer>();
        resetBoard = FindObjectOfType<ResetBoard>();
    }

    private void LateUpdate()
    {
        CardReader cardReader = cardDealer.players[cardDealer.currentPlayer].GetComponent<CardReader>();
        int currentPlayerScore = cardReader.currentScore;
        CardReader dealerCardReader = cardDealer.dealer.GetComponent<CardReader>();

        if (dealerCardReader.hand.Count == 2 && dealerCardReader.currentScore == 21)
        {
            dealerCardReader.isBlackjack = true;
            dealerBlackjackText.SetActive(true);
            if (!comparing)
            {
                StartCompareScores();
            }
        }
        else if (dealerCardReader.currentScore > 21)
        {
            dealerCardReader.isBust = true;
            dealerBustText.SetActive(true);
        }
        else
        {
            dealerCardReader.isBust = false;
            dealerCardReader.isBlackjack = false;
            dealerBustText.SetActive(false);
            dealerBlackjackText.SetActive(false);
        }

        if (!comparing)
        {
            if (currentPlayerScore > 21)
            {
                ShowBustText();
                cardReader.isBust = true;
            }
            else if (cardReader.hand.Count == 2 && currentPlayerScore == 21)
            {
                ShowBlackjackText();
                cardReader.isBlackjack = true;
            }
            else
            {
                ShowPlayerTurnImage();
                cardReader.isBlackjack = false;
                cardReader.isBust = false;
            }
        }
    }

    public void StartCompareScores()
    {
        StartCoroutine(CompareFinalScores());
    }

    IEnumerator CompareFinalScores()
    {
        comparing = true;

        for (int i = 0; i < cardDealer.players.Count; i++)
        {
            cardDealer.currentPlayer = i;
            int dealerScore = cardDealer.dealer.GetComponent<CardReader>().currentScore;
            CardReader currentCardReader = cardDealer.players[cardDealer.currentPlayer].GetComponent<CardReader>();
            CardReader dealerCardReader = cardDealer.dealer.GetComponent<CardReader>();
            int playerScore = currentCardReader.currentScore;

            foreach (GameObject card in currentCardReader.hand)
            {
                card.GetComponent<CardManager>().MakeCardFaceUp();
            }
            currentCardReader.UpdateScore();

            if (currentCardReader.isBlackjack)
            {
                ShowBlackjackText();
            }
            else if (currentCardReader.isBust)
            {
                ShowBustText();
            }
            else
            {
                if (!dealerCardReader.isBust)
                {
                    if (dealerScore < playerScore)
                    {
                        ShowWinText();
                    }
                    else if (dealerScore == playerScore)
                    {
                        ShowTieText();
                    }
                    else
                    {
                        ShowLoseText();
                    }
                }
                else
                {
                    if (!currentCardReader.isBust)
                    {
                        ShowWinText();
                    }
                }
            }
            
            yield return new WaitForSeconds(compareScoresDelay);
        }

        comparing = false;

        resetBoard.StartNewRound();
    }

    private void ShowPlayerTurnImage()
    {
        playerTurnImage.SetActive(true);
        bustText.SetActive(false);
        winText.SetActive(false);
        loseText.SetActive(false);
        tieText.SetActive(false);
        blackJackText.SetActive(false);
    }

    private void ShowBustText()
    {
        playerTurnImage.SetActive(false);
        bustText.SetActive(true);
        winText.SetActive(false);
        loseText.SetActive(false);
        tieText.SetActive(false);
        blackJackText.SetActive(false);
    }

    private void ShowWinText()
    {
        playerTurnImage.SetActive(false);
        bustText.SetActive(false);
        winText.SetActive(true);
        loseText.SetActive(false);
        tieText.SetActive(false);
        blackJackText.SetActive(false);
    }

    private void ShowLoseText()
    {
        playerTurnImage.SetActive(false);
        bustText.SetActive(false);
        winText.SetActive(false);
        loseText.SetActive(true);
        tieText.SetActive(false);
        blackJackText.SetActive(false);
    }

    private void ShowTieText()
    {
        playerTurnImage.SetActive(false);
        bustText.SetActive(false);
        winText.SetActive(false);
        loseText.SetActive(false);
        tieText.SetActive(true);
        blackJackText.SetActive(false);
    }

    private void ShowBlackjackText()
    {
        playerTurnImage.SetActive(false);
        bustText.SetActive(false);
        winText.SetActive(false);
        loseText.SetActive(false);
        tieText.SetActive(false);
        blackJackText.SetActive(true);
    }

}
