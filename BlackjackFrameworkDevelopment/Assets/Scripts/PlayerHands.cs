﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHands : MonoBehaviour
{
    public GameObject secondHand;

    [SerializeField] Vector3 twoHandsPosition;

    [SerializeField] List<GameObject> mainHandElements = new List<GameObject>();
    [SerializeField] List<GameObject> secondHandElements = new List<GameObject>();

    [SerializeField] Vector3 initialHandPosition;

    private void Start()
    {
        initialHandPosition = transform.localPosition;
    }

    public void SetNextHandActive()
    {
        for (int i = 0; i < mainHandElements.Count; i++)
        {
            mainHandElements[i].transform.localPosition = twoHandsPosition;
        }
        for (int i = 0; i < secondHandElements.Count; i++)
        {
            secondHandElements[i].SetActive(true);
        }
    }

    public void ReturnToInitalPosition()
    {
        for (int i = 0; i < mainHandElements.Count; i++)
        {
            mainHandElements[i].transform.localPosition = initialHandPosition;
        }
    }

    public void DeactivateSecondHand()
    {
        for (int i = 0; i < secondHandElements.Count; i++)
        {
            secondHandElements[i].SetActive(false);
        }
    }
}
