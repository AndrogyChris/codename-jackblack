﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] GameObject playerIndicator;
    [SerializeField] GameObject allButtons;
    [SerializeField] GameObject startRoundButton;

    [SerializeField] GameObject hitButton;
    [SerializeField] GameObject standButton;
    [SerializeField] GameObject splitButton;
    [SerializeField] GameObject doubleDownButton;
    [SerializeField] GameObject bustButton;

    private void Start()
    {
        SetDoubleDownButtonFalse();
        SetHitButtonFalse();
        SetSplitButtonFalse();

        SetPlayerIndicatorFalse();
        SetButtonsFalse();
    }

    public void SetHitButtonTrue()
    {
        hitButton.SetActive(true);
    }
    public void SetHitButtonFalse()
    {
        hitButton.SetActive(false);
    }


    public void SetStandButtonTrue()
    {
        standButton.SetActive(true);
    }
    public void SetStandButtonFalse()
    {
        standButton.SetActive(false);
    }


    public void SetSplitButtonTrue()
    {
        splitButton.SetActive(true);
    }
    public void SetSplitButtonFalse()
    {
        splitButton.SetActive(false);
    }


    public void SetDoubleDownButtonTrue()
    {
        doubleDownButton.SetActive(true);
    }
    public void SetDoubleDownButtonFalse()
    {
        doubleDownButton.SetActive(false);
    }


    public void SetPlayerIndicatorTrue()
    {
        playerIndicator.SetActive(true);
    }
    public void SetPlayerIndicatorFalse()
    {
        playerIndicator.SetActive(false);
    }


    public void SetButtonsTrue()
    {
        allButtons.SetActive(true);
    }
    public void SetButtonsFalse()
    {
        allButtons.SetActive(false);
    }

    public void StartRoundButtonTrue()
    {
        startRoundButton.SetActive(true);
    }
    public void StartRoundButtonFalse()
    {
        startRoundButton.SetActive(false);
    }
}
