﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDisplayUIFollow : MonoBehaviour
{
    [SerializeField] Transform target;

    Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
    }
    private void LateUpdate()
    {
        transform.position = mainCamera.WorldToScreenPoint(target.position);
    }
}
