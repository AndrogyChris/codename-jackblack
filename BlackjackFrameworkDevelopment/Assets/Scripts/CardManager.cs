﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    public int cardValue;

    [SerializeField] bool isAce;
    public string type;
    [SerializeField] GameObject faceSprite;
    [SerializeField] GameObject backSprite;

    CardDealer cardDealer;

    private void Awake()
    {
        cardDealer = FindObjectOfType<CardDealer>();
    }

    private void Start()
    {
        faceSprite = transform.GetChild(0).gameObject;
        backSprite = transform.GetChild(1).gameObject;
    }

    private void Update()
    {
        if (isAce)
        {
            CardReader reader = GetComponentInParent<CardReader>();

            if (reader != null && reader.currentScore > 21)
            {
                cardValue = 1;
                reader.UpdateScore();
            }
        }
    }

    public void ResetAce()
    {
        if (isAce)
        {
            cardValue = 11;
            GetComponentInParent<CardReader>().UpdateScore();
        }
    }

    public void ReturnAce()
    {
        cardValue = 11;
    }

    public void MakeCardFaceDown()
    {
        faceSprite.SetActive(false);
        backSprite.SetActive(true);
    }

    public void MakeCardFaceUp()
    {
        faceSprite.SetActive(true);
        backSprite.SetActive(false);
    }

}
