﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentPlayerUIFollow : MonoBehaviour
{
    private Transform target;
    private int playerToFollow;

    Camera mainCamera;
    CardDealer cardDealer;

    private void Start()
    {
        mainCamera = Camera.main;
        cardDealer = FindObjectOfType<CardDealer>();
    }
    private void LateUpdate()
    {
        playerToFollow = cardDealer.currentPlayer;
        target = cardDealer.players[playerToFollow];
        transform.position = mainCamera.WorldToScreenPoint(target.position);
    }

}
