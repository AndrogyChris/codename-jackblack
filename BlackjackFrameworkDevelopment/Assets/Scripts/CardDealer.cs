﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDealer : MonoBehaviour
{
    [SerializeField] Transform deck;
    [SerializeField] float offsetDistance;
    [SerializeField] float cardDealDelay = 0.5f;

    public List<Transform> players = new List<Transform>();
    public Transform dealer;

    private GameObject dealtCard;
    private float offSet;


    public bool dealing;
    public int currentPlayer;

    ButtonManager buttonManager;
    CompareScores compareScores;

    private void Awake()
    {
        //gets reference to ButtonManager and CompareScores they are regularly used
        buttonManager = FindObjectOfType<ButtonManager>();
        compareScores = FindObjectOfType<CompareScores>();
    }

    private void Update()
    {
        //Deals the second card to all players and dealer.
        if (!dealing && dealer.childCount == 1)
        {
            //Uses the same coroutine as the first deal.
            StartCoroutine(DealStartingCards());
        }

        //Starts the Dealer's turn as soon as the last player had clicked 'Stand'
        if (!dealing && currentPlayer > players.Count - 1)
        {
            //deactivates player buttons so they can't be used while they aren't supposed to
            buttonManager.SetButtonsFalse();
            //Dealer's turn
            StartCoroutine(DealerTurn());
            //Sets current player back to 0 so that when comparing scores they start at 0 and no IndexOutOfRange
            currentPlayer = 0;
        }

        //Allows hit button to appear if score is less than 21
        if (players[currentPlayer].GetComponent<CardReader>().currentScore < 21)
        {
            buttonManager.SetHitButtonTrue();
        }
        else
        {
            //if score is 21 or higher, player cannot hit
            buttonManager.SetHitButtonFalse();
        }

        //Activates Splitting if player has a double
        if (players[currentPlayer].GetComponent<CardReader>().hasDouble)
        {
            buttonManager.SetSplitButtonTrue();
        }
        else
        {
            buttonManager.SetSplitButtonFalse();
        }

        //If the player has an initial score of 9, 10, or 11 they can double down. Button activated for that
        if (players[currentPlayer].GetComponent<CardReader>().canDoubleDown)
        {
            buttonManager.SetDoubleDownButtonTrue();
        }
        else
        {
            buttonManager.SetDoubleDownButtonFalse();
        }
    }

    public void StartRound()
    {
        //Start button is deactivated as soon as it is clicked
        buttonManager.StartRoundButtonFalse();
        //starts dealing the first round of cards
        StartCoroutine(DealStartingCards());
    }

    //Coroutine so that there is a delay between card deals
    IEnumerator DealStartingCards()
    {
        //for loop to deal a single card to each player in List 'players'
        for (int i = 0; i < players.Count; i++)
        {
            //sets dealing to true so that a second coroutine isn't started
            dealing = true;
            //makes the current player i so that all players will be dealt to
            currentPlayer = i;
            //calls the method that will give the current player a card
            DealToPlayer();
            //wait until next card is dealt
            yield return new WaitForSeconds(cardDealDelay);
        }
        //after all players are dealt to, one card is given to the dealer
        DealToDealer();

        //checks is the dealer has a second card
        if (dealer.childCount == 2)
        {
            //second card is dealt face down
            DealCardFaceDown();
            //dealer true score is not displayed
            dealer.GetComponent<CardReader>().DecreaseScore();

            //if dealer has second card then players have access to turn buttons
            buttonManager.SetButtonsTrue();
            //activates the display that shows which player's turn it is
            buttonManager.SetPlayerIndicatorTrue();
        }

        //after dealing to every player and dealer twice, the player's can now take their turns. Starting with the first one 
        currentPlayer = 0;
        yield return new WaitForSeconds(cardDealDelay);
        dealing = false;
        
    }


    //Coroutine that executes the dealer's turn
    IEnumerator DealerTurn()
    {
        //first makes the face down card face up, and updates the score
        dealer.GetChild(dealer.childCount - 1).GetComponent<CardManager>().MakeCardFaceUp();
        dealer.GetComponent<CardReader>().UpdateScore();

        //waits between dealing cards to dealer
        yield return new WaitForSeconds(cardDealDelay);

        //while the dealer has a score of less than 17, keep drawing cards
        while (dealer.GetComponent<CardReader>().currentScore < 17)
        {
            DealToDealer();

            yield return new WaitForSeconds(cardDealDelay);
        }

        //once dealer has drawn cards neccessary, move onto comparing scores phase
        compareScores.StartCompareScores();
    }

    private void DealToPlayer()
    {
        //selects card that will be dealt
        SelectCard();
        //sets the parent of the selected card to the appropriate player
        dealtCard.transform.SetParent(players[currentPlayer]);
        //calculates the offset based on how many other cards are in hand
        offSet = (players[currentPlayer].childCount - 1) * offsetDistance;
        //renders the sprite in front of every other card
        dealtCard.GetComponentInChildren<SpriteRenderer>().sortingOrder = players[currentPlayer].childCount - 1;
        //sets the card's local position, adding the offset to the Y position.
        dealtCard.transform.localPosition = new Vector3(0, 0 - offSet, 0);
        //Updates the current player's score
        players[currentPlayer].GetComponent<CardReader>().UpdateScore();
    }

    private void DealToDealer()
    {
        //selects card that will be dealt
        SelectCard();
        //parents card to dealer
        dealtCard.transform.SetParent(dealer);
        //calculates the offset based on how many other cards are in hand
        offSet = (dealer.childCount - 1) * offsetDistance;
        //renders the sprite in front of every other card
        dealtCard.GetComponentInChildren<SpriteRenderer>().sortingOrder = dealer.childCount - 1;
        //sets the card's local position, adding the offset to the X position.
        dealtCard.transform.localPosition = new Vector3(0 + offSet, 0, 0);
        //Updates the dealer's score
        dealer.GetComponent<CardReader>().UpdateScore();
    }


    //Calls the funtion that makes the card face down, so that it is not seen
    private void DealCardFaceDown()
    {
        dealtCard.GetComponent<CardManager>().MakeCardFaceDown();
    }

    //Draws another card to the current player
    public void Hit()
    {
        DealToPlayer();
    }

   
    public void DoubleDown()
    {
        //draws a card for the current player. 
        DealToPlayer();
        //makes sure that the score isn't updated
        players[currentPlayer].GetComponent<CardReader>().DecreaseScore();
        //makes the card face down
        DealCardFaceDown();
        //skips to next player's turn
        NextPlayerTurn();
    }


    public void Split()
    {
        //gets references to PlayerHands script
        PlayerHands handComponent = players[currentPlayer].GetComponent<PlayerHands>();
        //Sets second hand active
        handComponent.SetNextHandActive();
        //inserts new player into 'players' List immediately after current player
        players.Insert(currentPlayer + 1, handComponent.secondHand.transform);
        //selects latest drawn card to move to next hand
        GameObject movedCard = players[currentPlayer].GetChild(players[currentPlayer].childCount - 1).gameObject;
        //parents this card to the new hand
        movedCard.transform.SetParent(handComponent.secondHand.transform);
        //moves this card to the new hand
        movedCard.transform.localPosition = new Vector3(0, 0, 0);
        //adjusts sorting order so that it is the lowest card
        movedCard.GetComponentInChildren<SpriteRenderer>().sortingOrder = 0;
        //sets first hand to has split to true so the player cannot split again
        players[currentPlayer].GetComponent<CardReader>().hasSplit = true;
        //sets second hand to has split to true so the player cannot split again
        handComponent.secondHand.GetComponent<CardReader>().hasSplit = true;
        //resets any aces that have been convertes to a value of 1 on both the main hand and the new hand
        players[currentPlayer].GetChild(0).GetComponent<CardManager>().ResetAce();
        handComponent.secondHand.transform.GetChild(0).GetComponent<CardManager>().ResetAce();
        //updates both hands' scores
        players[currentPlayer].GetComponent<CardReader>().UpdateScore();
        handComponent.secondHand.GetComponent<CardReader>().UpdateScore();
    }

    //remove split hands when resetting the board
    public void UnSplitHand()
    {
        //gets reference to PlayerHands scripts
        PlayerHands handComponent = players[currentPlayer].GetComponent<PlayerHands>();
        //makes has split false so that the player can split next round
        players[currentPlayer].GetComponent<CardReader>().hasSplit = false;
        handComponent.secondHand.GetComponent<CardReader>().hasSplit = false;
        //Get reference to second hand's cardReader
        CardReader cardReader = players[currentPlayer + 1].GetComponent<CardReader>();
        //sends cards in second hand back to the deck
        foreach (GameObject card in cardReader.hand)
        {
            card.transform.SetParent(deck);
            card.transform.localPosition = new Vector3(0, 0, 0);
        }
        //removes second hand from the List of players
        players.RemoveAt(currentPlayer + 1);
        //deactivates second hand game objects
        handComponent.DeactivateSecondHand();
        //returns the main hand back to where it belongs
        handComponent.ReturnToInitalPosition();
    }

    //funtion used to increase current player, so that the next player has a turn
    public void NextPlayerTurn()
    {
        currentPlayer++;
    }

    //chooses a random card from the deck parent object, making so that there is no need to shuffle the deck
    private void SelectCard()
    {
        dealtCard = deck.GetChild(UnityEngine.Random.Range(0, deck.childCount)).gameObject;
    }
}
